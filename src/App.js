import './App.css';
import EditStudent from './EditStudent';

function App() {
  return (
    <div className="App">
      <EditStudent />
    </div>
  );
}

export default App;
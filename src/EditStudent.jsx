import { useState } from 'react';

export default function EditStudent() {

    const defaultStudent = {
        studentid: '',
        name: '',
        city: '',
        email: '',
        phone: ''
    };

    const [student, setStudent] = useState(defaultStudent);

    // Check if data is not loaded
    if(student.studentid === '')
    {
        fetch('https://joni-koulu-default-rtdb.europe-west1.firebasedatabase.app/react-test.json')
        .then(response => response.json())
        .then(data => {
            setStudent(data);
        })
        .catch(error => console.error(error));
    }

    function save(event)
    {
        event.preventDefault();

        const details = {
            studentid: event.target.studentid.value,
            name: event.target.name.value,
            city: event.target.city.value,
            email: event.target.email.value,
            phone: event.target.phone.value
          };
        
        const options = {
            method: 'PATCH',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(details)
          };

          console.log("Saved data");

          fetch('https://joni-koulu-default-rtdb.europe-west1.firebasedatabase.app/react-test.json', options)
            .then(response => response.json())
            .then(data => {
                console.log(data);
            })
            .catch(error => {
                console.error(error);
            });
    }

    return (
        <>
        <form onSubmit={save}>
            <label>Studentid  </label><input type="text" id="studentid" defaultValue={student.studentid}/><br></br>
            <label>Name  </label><input type="text" id="name" defaultValue={student.name}/><br></br>
            <label>City  </label><input type="text" id="city" defaultValue={student.city}/><br></br>
            <label>Email  </label><input type="email" id="email" defaultValue={student.email}/><br></br>
            <label>Phone  </label><input type="phone" id="phone" defaultValue={student.phone}/><br></br>
            <button type="submit">Save</button>
        </form>
        </>
    );
}